
Contents
  - Drupal Module Install
  - Install node.js
  - Start the node.js application

What this module IS & what it is not
-----------------------
This module provides an API for other developers to run realtime notifications,
this module does not provide the interface for users to run the notifications,
because there does need to be some setup on the server side.


How to install & Setup
-----------------------

Install Drupal module as normal.

You will now need to start the node.js script,


  cd into
  /realtime_notification_system_api/nodejs/app/node_modules/express

  You will now need to start the node.js server, this can be done by running
  node index.js

  You will next need to install forever, which can be done by following the
  information in this link -> https://www.npmjs.com/package/forever

  When you have index.js running the application will be ready so send
  notifications.

How to Use
-----------------------
realtime_notifications_system_api_send_message($message)

$message is a string
returns an array of whether the message sent successfully or not

If the module has been installed correctly then on the client side the javascript
object io('http://localhost:3000') should of triggered an event.

You will need to run the function above with a message along with creating a javascript
file which has this,

  e.g. can be found at /realtime_notification_system_api/js/notification.client.js

  var socket = io('http://localhost:3000');

  socket.on('notification', function(notification){

    // you put your action here!
  });

you will then be able to act on these events.
